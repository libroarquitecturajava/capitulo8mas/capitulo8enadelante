package com.arquitectura;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Table;
import javax.persistence.TypedQuery;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
@Table(name="categorias")
public class Categoria {
	@Id
	@Column(name="id_categoria")
	private int id;
	
	@Column(name="descripcion")
	private String descripcion;
	
	@OneToMany
	@JoinColumn(name="categoria")
	private List<Libro> listaDelibro;
	
	public int hashCode() {
		return id;
	}
	
	
	public Categoria() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Categoria(String descripcion) {
		super();
		this.descripcion = descripcion;
	}


	@Override
	public boolean equals(Object o) {
		int categoriaId = ((Categoria)o).getId();
		return (id == categoriaId);
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Libro> getListaDelibro() {
		return listaDelibro;
	}

	public void setListaDelibro(List<Libro> listaDelibro) {
		this.listaDelibro = listaDelibro;
	}
	
	
	
}
