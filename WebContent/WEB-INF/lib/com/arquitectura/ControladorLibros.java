package com.arquitectura;

import java.io.IOException;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;

import com.arquitecturajava.controlador.acciones.Accion;
import com.arquitecturajava.controlador.acciones.BorrarLibroAccion;
import com.arquitecturajava.controlador.acciones.FiltrarLibrosPorCategoriaAccion;
import com.arquitecturajava.controlador.acciones.FormularioEditarLibroAccion;
import com.arquitecturajava.controlador.acciones.FormularioInsertarLibroAccion;
import com.arquitecturajava.controlador.acciones.InsertaLibroAccion;
import com.arquitecturajava.controlador.acciones.ModificarLibroAccion;
import com.arquitecturajava.controlador.acciones.MostrarLibrosAccion;

import javax.servlet.ServletException;

/*
 * El principio OCP y el Controlador 
 * para que este ya no se modifica cada vez que haya una nueva operacion
 * y estas se realicen desde la clase heredadas de accion 
 * 
 * 
 * */
public class ControladorLibros extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	public ControladorLibros() {
		super();
	}
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response ) throws 
	ServletException, IOException{
		Accion accion = null ;
		RequestDispatcher despachador = null;
		String url = request.getPathInfo();
	accion = Accion.getAccion(url.substring(1, url.length()-3));
		despachador = request.getRequestDispatcher(accion.ejecutar(request, response));
		despachador.forward(request,response);
	}
}
