package com.arquitecturajava.controlador.acciones;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.arquitectura.Categoria;
import com.arquitectura.Libro;
import com.arquitecturajava.dao.CategoriaDAO;
import com.arquitecturajava.dao.DAOAbstractFactory;
import com.arquitecturajava.dao.DAOFactory;
import com.arquitecturajava.dao.LibroDAO;
import com.arquitecturajava.dao.jpa.CategoriaDAOJPAImplGeneric;
import com.arquitecturajava.dao.jpa.LibroDAOJPAImpJPAGeneric;
import com.arquitecturajava.servicios.ServiciosLibros;
import com.arquitecturajava.servicios.impl.ServicioLibrosImpl;

public class ModificarLibroAccion extends Accion{

	public  String ejecutar(HttpServletRequest request, HttpServletResponse response) {
		String cadena = "/controladorLibros/MostrarLibros.do";
		String isbn = request.getParameter("isbn");
		String titulo = request.getParameter("titulo");
		String categoria = request.getParameter("categoria");
		
		ServiciosLibros servicio = (ServiciosLibros) getBean("servicioLibros", request);
		
		Libro libro = servicio.buscarLibroPorClave(isbn);
		libro.setTitulo(titulo);
		libro.setCategoria( servicio.buscarCategoriaPorClave( 
				Integer.parseInt( categoria ) ) );
		servicio.salvarLibros(libro);
		return cadena;
	}
	
}
