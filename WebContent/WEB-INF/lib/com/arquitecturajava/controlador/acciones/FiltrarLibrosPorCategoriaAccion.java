package com.arquitecturajava.controlador.acciones;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.arquitectura.Categoria;
import com.arquitectura.Libro;
import com.arquitecturajava.dao.CategoriaDAO;
import com.arquitecturajava.dao.DAOAbstractFactory;
import com.arquitecturajava.dao.DAOFactory;
import com.arquitecturajava.dao.LibroDAO;
import com.arquitecturajava.dao.jpa.CategoriaDAOJPAImplGeneric;
import com.arquitecturajava.dao.jpa.LibroDAOJPAImpJPAGeneric;
import com.arquitecturajava.servicios.ServiciosLibros;
import com.arquitecturajava.servicios.impl.ServicioLibrosImpl;

public class FiltrarLibrosPorCategoriaAccion extends Accion {

	public  String ejecutar(HttpServletRequest request, 
			HttpServletResponse response) {
		
		String cadena = "/controladorLibros/MostrarLibros.do";
		List<Libro> listaDeLibros=null;
			ServiciosLibros servicio = (ServiciosLibros) getBean(
					"servicioLibros",request);
			
		List<Categoria> listaDeCategorias = servicio.buscarCategoriasLibros();
		if(request.getParameter("categoria")== null || request.getParameter("categoria").
				equals("seleccionar")) {
			listaDeLibros = servicio.buscarTodosLosLibros();
		} else { 
			listaDeLibros = servicio.buscarLibrosPorCategoria(
					Integer.parseInt( request.getParameter("categoria") ) );
		}
		request.setAttribute("listaDeLibros", listaDeLibros);
		request.setAttribute("listaDeCategorias", listaDeCategorias);
		return cadena;
	}
}
