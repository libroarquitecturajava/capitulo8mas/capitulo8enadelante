package com.arquitecturajava.controlador.acciones;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Esta clase permite por medio de la herencia, implementar el princio OCP
 * Open closed principe que consiste en estar cerrado a las modificaciones 
 * y abierto a las extensibilidad y por otro lado implementando el patron command 
 * el cual se emplea mediante un metodo abstracto que permita realizar la accion 
 * correspondiente a cada clase hija.
 * El principio OCP y el patron Command mediante el metodo ejecutar
 * Cada clase hija de accion tiene una definicion del metodo (patron commando ) ejecutar diferente
 * Creacion de una accion princpipal 
 * Crear una jerarquia de acciones
 * 
 * }
 * 
 * */
public abstract class Accion {

	public abstract String ejecutar( HttpServletRequest request, HttpServletResponse response);
	
	
	public static Accion getAccion(String tipo ) {
		Accion accion = null; 
		try {
			
			String clase = Accion.class.getPackage()+"."+tipo+"Accion";
			clase = clase.replaceAll("package", "").replaceAll(" ", "");
		accion = (Accion) Class.forName(clase).newInstance();
		}catch(InstantiationException e) {
			e.printStackTrace();
		}catch(IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return accion;
	}
	
	public  Object getBean(String nombreBean, HttpServletRequest request) {
		WebApplicationContext factoria = WebApplicationContextUtils.
				getRequiredWebApplicationContext(request.getSession().
						getServletContext());
		return factoria.getBean(nombreBean); 
	}
}
