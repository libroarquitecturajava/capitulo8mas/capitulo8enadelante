package com.arquitecturajava.controlador.acciones;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.arquitectura.Categoria;
import com.arquitectura.Libro;
import com.arquitecturajava.dao.CategoriaDAO;
import com.arquitecturajava.dao.DAOAbstractFactory;
import com.arquitecturajava.dao.DAOFactory;
import com.arquitecturajava.dao.LibroDAO;
import com.arquitecturajava.dao.jpa.CategoriaDAOJPAImplGeneric;
import com.arquitecturajava.servicios.ServiciosLibros;
import com.arquitecturajava.servicios.impl.ServicioLibrosImpl;

public class FormularioInsertarLibroAccion extends Accion {

	public  String ejecutar(HttpServletRequest request, HttpServletResponse response) {
		String cadena = "/FormularioInsertarLibro.jsp";
		List<Categoria> listaDeCategorias=null;
		ServiciosLibros servicio = (ServiciosLibros) getBean("servicioLibros",request);
		listaDeCategorias = servicio.buscarCategoriasLibros();
		request.setAttribute("listaDeCategorias", listaDeCategorias);
		return cadena;
	}
	
}
