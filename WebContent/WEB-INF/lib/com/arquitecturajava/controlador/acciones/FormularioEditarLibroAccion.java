package com.arquitecturajava.controlador.acciones;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.arquitectura.Categoria;
import com.arquitectura.Libro;
import com.arquitecturajava.dao.CategoriaDAO;
import com.arquitecturajava.dao.DAOAbstractFactory;
import com.arquitecturajava.dao.DAOFactory;
import com.arquitecturajava.dao.LibroDAO;
import com.arquitecturajava.dao.jpa.CategoriaDAOJPAImplGeneric;
import com.arquitecturajava.dao.jpa.LibroDAOJPAImpJPAGeneric;
import com.arquitecturajava.servicios.ServiciosLibros;
import com.arquitecturajava.servicios.impl.ServicioLibrosImpl;

public class FormularioEditarLibroAccion extends Accion {

	public  String ejecutar(HttpServletRequest request,
			HttpServletResponse response) {
		String cadena = "/FormularioEditarLibro.jsp";
		String isbn = request.getParameter("isbn");
		ServiciosLibros servicio = (ServiciosLibros) getBean("servicioLibros",request);
		List<Categoria> listaDeCategorias = servicio.buscarCategoriasLibros();
		Libro libro = servicio.buscarLibroPorClave(request.getParameter("isbn"));
		request.setAttribute("listaDeCategorias", listaDeCategorias);
		request.setAttribute("libro", libro);
		return cadena;
	}
	
}
