package com.arquitecturajava.controlador.acciones;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.arquitectura.Categoria;
import com.arquitectura.Libro;
import com.arquitecturajava.dao.CategoriaDAO;
import com.arquitecturajava.dao.DAOAbstractFactory;
import com.arquitecturajava.dao.DAOFactory;
import com.arquitecturajava.dao.LibroDAO;
import com.arquitecturajava.dao.jpa.CategoriaDAOJPAImplGeneric;
import com.arquitecturajava.dao.jpa.LibroDAOJPAImpJPAGeneric;
import com.arquitecturajava.servicios.ServiciosLibros;
import com.arquitecturajava.servicios.impl.ServicioLibrosImpl;

public class MostrarLibrosAccion extends Accion{

	public  String ejecutar(HttpServletRequest request, HttpServletResponse response) {
		String cadena = "/MostrarLibros.jsp";
		/*Cargan los datos de la pagina MostrarLibros*/
		List<Libro> listaDeLibros;
		List<Categoria> listaDeCategorias;
		
		if(request.getAttribute("listaDeLibros") == null | request.getAttribute("listaDeCategorias") == null) {
			ServiciosLibros servicio = (ServiciosLibros) getBean("servicioLibros",request);
			
		listaDeLibros = servicio.buscarTodosLosLibros();
		
		 listaDeCategorias = servicio.buscarCategoriasLibros();
		
		/* Almacena ambas listas en el objeto request el cual es accesible 
		 * para el servlet controlador como para las paginas jsp fingiendo 
		 * como intermediario entre las paginas*/ 
		}else {
			listaDeLibros = (List<Libro>) request.getAttribute("listaDeLibros");
			listaDeCategorias = (List<Categoria>) request.getAttribute("listaDeCategorias");
		}
		request.setAttribute("listaDeLibros",listaDeLibros);
		request.setAttribute("listaDeCategorias", listaDeCategorias);
		
		/*Las siguientes lineas permiten a las paginas obtener los objetos 
		 * de los libros, eliminando la persistencia de los objetos en las 
		 * paginas sirviendo como intermediario*/
		return cadena;
	}
}
