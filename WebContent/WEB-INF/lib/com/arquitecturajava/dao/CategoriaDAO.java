package com.arquitecturajava.dao;

import java.util.List;
import com.arquitectura.Categoria;


public interface CategoriaDAO {
	public abstract Categoria buscarPorClave(Integer id);
	public abstract  void insertar(Categoria categoria);
	public abstract List<Categoria> buscarTodos();
	public abstract void borrar(Categoria categoria);
}
