package com.arquitecturajava.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import com.arquitectura.Categoria;
import com.arquitecturajava.dao.CategoriaDAO;

public class CategoriaDAOJPAImpl implements CategoriaDAO{

	public  List<Categoria> buscarTodos(){
		List<Categoria>  lista = null;
		String cadConsulta = "FROM Categoria categorias";
		
		EntityManagerFactory factoriaSesion = JPAHelper.getJPAFactory();
		EntityManager manager = factoriaSesion.createEntityManager();
		try {
		TypedQuery consulta = manager.createQuery(cadConsulta, Categoria.class);
		lista = consulta.getResultList();
		}finally {
			manager.close();
		}
		return lista;
	}
	
	public  void insertar(Categoria categoria) {
		EntityManagerFactory factoriaSession = JPAHelper.getJPAFactory(); 
		EntityManager manager = factoriaSession.createEntityManager();
		EntityTransaction tx = null;
		
		try {
		tx = manager.getTransaction();
		tx.begin();
		manager.persist(categoria);
		tx.commit();
		}catch(PersistenceException e) {
			tx.rollback();
		}finally {
			manager.close();
		}
	}
	
	public  Categoria buscarPorClave(Integer id) {
		Categoria categoria = null;
		EntityManagerFactory factoriaSesion = JPAHelper.getJPAFactory();
		EntityManager manager = factoriaSesion.createEntityManager();
		TypedQuery<Categoria> consulta = manager.createQuery(
				"SELECT C FROM Categoria C WHERE C.id=?1",Categoria.class);
		try {
			consulta.setParameter(1, id.intValue());
			categoria = consulta.getSingleResult();	
		}finally {
			manager.close();
		}
		
	 return categoria ;
	}
	
	public  void borrar(Categoria categoria) {
		EntityManagerFactory factoriaSesion = JPAHelper.getJPAFactory();
		EntityManager manager = factoriaSesion.createEntityManager();
		EntityTransaction tx = null;
		
		try {
			tx = manager.getTransaction();
			tx.begin();
			manager.remove(categoria);
			tx.commit();
		}catch(PersistenceException e ) {
			tx.rollback();
			throw e;
		}finally {
			manager.close();
		}
		
	}
}
