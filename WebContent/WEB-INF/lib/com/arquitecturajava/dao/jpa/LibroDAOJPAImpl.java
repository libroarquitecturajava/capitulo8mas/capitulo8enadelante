package com.arquitecturajava.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import com.arquitectura.Categoria;
import com.arquitectura.Libro;
import com.arquitecturajava.dao.LibroDAO;

public class LibroDAOJPAImpl implements LibroDAO{
		
	@SuppressWarnings("unchecked")
	public static List<String> buscarTodasLasCategorias() 
	{
		List<String> listaDeCategorias = null;
		
		EntityManagerFactory factoriaSesion = JPAHelper.getJPAFactory();
		EntityManager manager = factoriaSesion.createEntityManager();
		TypedQuery consulta = manager.createQuery(
				"SELECT DISTINCT I.categoria.descripcion FROM Libro I ", String.class);
		try {
		listaDeCategorias = consulta.getResultList();
		}finally{
			manager.close();
		}
		return listaDeCategorias;
	}
	
	public  void insertar(Libro libro) {
		EntityManagerFactory factoriaSession = JPAHelper.getJPAFactory(); 
		EntityManager manager = factoriaSession.createEntityManager();
		EntityTransaction tx = null;
		
		try {
		tx = manager.getTransaction();
		tx.begin();
		manager.persist(libro);
		tx.commit();
		}catch(PersistenceException e) {
			tx.rollback();
		}finally {
			manager.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public  List<Libro> buscarTodos() 
	{
		EntityManagerFactory  factoriaSesion = JPAHelper.getJPAFactory();
		EntityManager manager = factoriaSesion.createEntityManager();
		TypedQuery<Libro> consulta = manager.
				createQuery("SELECT I FROM Libro I JOIN FETCH I.categoria",
						Libro.class);
		List<Libro> listaDeLibros = null;
		
		try {
			listaDeLibros = consulta.getResultList();
		
		}finally{
			manager.close();
		}
		return listaDeLibros;
	}
	
	public void borrar(Libro libro) 
	{
		EntityManagerFactory factoriaSesion = JPAHelper.getJPAFactory();
		EntityManager manager = factoriaSesion.createEntityManager();
		EntityTransaction tx = null;
		
		try { 
		tx = manager.getTransaction();
		tx.begin();
		manager.remove(manager.merge(libro));
		tx.commit();
		}catch(PersistenceException e){
			manager.getTransaction().rollback();
		}finally {
		manager.close();
		}
	}
	
	public void salvar(Libro libro)  {
		EntityManagerFactory factoriaSesion = JPAHelper.getJPAFactory();
		EntityManager manager = factoriaSesion.createEntityManager();
		EntityTransaction tx = null;
		
		try {
			tx = manager.getTransaction();
			tx.begin();
			manager.merge(libro);
			tx.commit();
		}finally {
			manager.close();
		}
	}
	
	public  Libro buscarPorClave(String isbn) 
	{
		EntityManagerFactory factoriaSesion = JPAHelper.getJPAFactory();
		EntityManager manager = factoriaSesion.createEntityManager();
		TypedQuery<Libro> consulta = manager.createQuery(
				"Select I from Libro I JOIN FETCH I.categoria where I.isbn=?1",
				Libro.class);
		Libro libro = null;
		try{
			consulta.setParameter(1, isbn);
			
			libro = consulta.getSingleResult();
		}finally {
			manager.close();
		}
		return libro;
	}
	
	@SuppressWarnings("unchecked")
	public  List<Libro> buscarPorCategoria(Categoria categoria) 
	{
		List<Libro>	listaDeLibros = null;
		EntityManagerFactory factoriaSesion = JPAHelper.getJPAFactory();
		EntityManager manager = factoriaSesion.createEntityManager();
		
		TypedQuery<Libro> consulta = manager.createQuery(
				"Select I from Libro I JOIN FETCH I.categoria where I.categoria=?1",
				Libro.class);
		try {
		consulta.setParameter(1,categoria);
		listaDeLibros = consulta.getResultList();
		}finally {
			manager.close();
		}
		return listaDeLibros;
	}
}
