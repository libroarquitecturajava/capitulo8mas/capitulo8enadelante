package com.arquitecturajava.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import com.arquitectura.Categoria;
import com.arquitectura.Libro;
import com.arquitecturajava.dao.LibroDAO;


public class LibroDAOJPAImpJPAGeneric extends GenericDAOJPAImpl<Libro, String> 
implements LibroDAO{

	@SuppressWarnings("unchecked")
	public  List<Libro> buscarPorCategoria(Categoria categoria) 
	{
		List<Libro>	listaDeLibros = null;
		EntityManagerFactory factoriaSesion = JPAHelper.getJPAFactory();
		EntityManager manager = factoriaSesion.createEntityManager();
		
		TypedQuery<Libro> consulta = manager.createQuery(
				"Select I from Libro I JOIN FETCH I.categoria where I.categoria=?1",
				Libro.class);
		try {
		consulta.setParameter(1,categoria);
		listaDeLibros = consulta.getResultList();
		}finally {
			manager.close();
		}
		return listaDeLibros;
	}
	
	/*Este metodo se sobre carg ya que en el DAO generico no obtiene el objeto
	 *  Categoria con datos, al usar el fetch le decimos a JPA que lo llene */
	public List<Libro> buscarTodos(){
		TypedQuery<Libro> consulta = JPAHelper.getJPAFactory().
				createEntityManager().createQuery(
						"SELECT I FROM Libro I JOIN FETCH I.categoria"
						, Libro.class);
		return consulta.getResultList();
	}
	
}
