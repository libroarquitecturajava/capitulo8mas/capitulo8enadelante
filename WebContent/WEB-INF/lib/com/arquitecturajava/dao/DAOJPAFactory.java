package com.arquitecturajava.dao;

import com.arquitecturajava.dao.jpa.CategoriaDAOJPAImpl;
import com.arquitecturajava.dao.jpa.CategoriaDAOJPAImplGeneric;
import com.arquitecturajava.dao.jpa.LibroDAOJPAImpJPAGeneric;
import com.arquitecturajava.dao.jpa.LibroDAOJPAImpl;

/* Esta clase se utiliza es creada para seguir el patron de diseno
 * abstract Factory el cual consiste en crear familias de clases 
 * que tengan una similitud en este caso tenemos que esta factoria 
 * separa las clases de persistencia que implementan JPA, y por otra parte
 * se crea una factoria que permita elegir una de las dos familias en la 
 * convinacion que sea pertinente*/

public class DAOJPAFactory implements DAOFactory{
	
	public CategoriaDAO getCategoriaDAO() {
		return new CategoriaDAOJPAImplGeneric();
	}
	
	public LibroDAO getLibroDAO() {
		return new LibroDAOJPAImpJPAGeneric();
	}
}
