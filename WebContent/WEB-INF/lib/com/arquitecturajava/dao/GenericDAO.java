package com.arquitecturajava.dao;

import java.io.Serializable;
import java.util.List;


public interface GenericDAO<T,Id extends Serializable> {
	public  T buscarPorClave(Id id);
	public  List<T> buscarTodos();
	public void salvar(T objeto);
	public  void borrar(T objeto);
}
