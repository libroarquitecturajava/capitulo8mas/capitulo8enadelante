package com.arquitecturajava.dao;

import com.arquitecturajava.dao.hibernate.CategoriaDAOHibernateImpl;
import com.arquitecturajava.dao.hibernate.LibroDAOHibernateImpl;

/* Esta clase se utiliza es creada para seguir el patron de diseno
 * abstract Factory el cual consiste en crear familias de clases 
 * que tengan una similitud en este caso tenemos que esta factoria 
 * separa las clases de persistencia que implementan Hibernate, y por otra parte
 * se crea una factoria que permita elegir una de las dos familias en la 
 * convinacion que sea pertinente*/

public class DAOHibernateFactory implements DAOFactory {
	
	public CategoriaDAO getCategoriaDAO() {
		return new CategoriaDAOHibernateImpl();
	}
	
	public LibroDAO getLibroDAO() {
		return new  LibroDAOHibernateImpl();
		
	}
	
}
