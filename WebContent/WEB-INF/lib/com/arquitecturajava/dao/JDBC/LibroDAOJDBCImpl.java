package com.arquitecturajava.dao.JDBC;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.arquitectura.Categoria;
import com.arquitectura.Libro;
import com.arquitecturajava.dao.LibroDAO;
import com.arquitecturajava.dao.jpa.LibroDAOJPAImpl;

public class LibroDAOJDBCImpl implements LibroDAO{
		
	private static final Logger log = Logger.getLogger(LibroDAOJDBCImpl.class.getPackage().getName());
	
	 
	
	public  void insertar(Libro libro) {
		String consulta = "INSERT INTO Libros(isbn,Titulo,Categoria) values("
				+ " '"+libro.getIsbn()+"' ,'"+libro.getTitulo()+"',"+ libro.getCategoria().getId()+")";
		try {
			DataBaseHelper<Libro> dataHelper= new DataBaseHelper<Libro>();
			dataHelper.modificarRegistro(consulta);
			
		}catch(SQLException e) {
			throw new DataBaseException("Error al insertar un libro");	
		}
	}
	public  void borrar(Libro libro) {
		String consulta = "DELETE FROM Libros where isbn='"+libro.getIsbn()+
				"'";
		try {
			DataBaseHelper<Libro> dataHelper = new DataBaseHelper<Libro>();
			dataHelper.modificarRegistro(consulta);
		}catch(SQLException e){
				throw new DataBaseException("Error al borrar libro");
			}
		}
	
	public  List<Libro> buscarPorCategoria(Categoria categoria){
		List<Libro> listaBook = null;
		String consulta = "SELECT * FROM Libros where categoria="
		+ categoria.getId();
		
		DataBaseHelper<Libro> dataHelper = new DataBaseHelper<Libro>();
		dataHelper.seleccionarRegistros(consulta,Libro.class);
		
		return listaBook;
	}
	
	public  Libro buscarPorClave(String isbn) {
		
		List<Libro> libros = null;
		String consulta = "SELECT isbn, titulo, categoria from Libros "
				+ " where isbn='"+isbn+"'";
		DataBaseHelper<Libro> dataHelper = new DataBaseHelper<Libro>();
		dataHelper.seleccionarRegistros(consulta,Libro.class);
		return libros.get(0);
	}
	
	public  void salvar(Libro libro) {
		String consultaUp = "update libros set titulo='"+ libro.getTitulo()+
				", categoria="+libro.getCategoria().getId()+
					" where isbn='"+ libro.getIsbn()+"'";
			try {
				DataBaseHelper<Libro> dataHelper = new DataBaseHelper<Libro>();
				dataHelper.modificarRegistro(consultaUp);
			}catch(SQLException e){
				throw new DataBaseException("Error al actualiar libro");
				}
	}
	
	public   List<Libro> buscarTodos(){
		String consulta = "SELECT isbn, Titulo, Categoria from Libros";
		List<Libro> listaLibros = null;
		
		 DataBaseHelper<Libro> dataHelper = new DataBaseHelper<Libro>();
		 listaLibros = dataHelper.seleccionarRegistros(consulta,Libro.class);
		return listaLibros;
	}
}
