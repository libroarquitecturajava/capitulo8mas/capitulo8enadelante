package com.arquitecturajava.dao.JDBC;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.arquitectura.Categoria;
import com.arquitecturajava.dao.CategoriaDAO;

public class CategoriaDAOJDBCImpl implements CategoriaDAO {
	
private static final Logger log = Logger.
			getLogger(CategoriaDAOJDBCImpl.class.getPackage().getName());
	
	
	
	public  Categoria buscarPorClave(Integer id) {
		String consulta = "SELECT id_categoria, decripcion FROM categorias "
				+ " WHERE id_categoria="+ id.intValue();
		List<Categoria> listaCategorias = null;
		
		DataBaseHelper<Categoria> dataHelper= new DataBaseHelper<Categoria>();
		listaCategorias = dataHelper.
				seleccionarRegistros(consulta, Categoria.class);
		return listaCategorias.get(0);
	}
	
	public   void insertar(Categoria categoria) {
		String consulta = "INSERT INTO categorias(id_categoria,descripcion) "
				+ "VALUES("+categoria.getId()+",'"+
				categoria.getDescripcion()+"')";
		DataBaseHelper<Categoria> dataHelper= new DataBaseHelper<Categoria>();
		try {
		dataHelper.modificarRegistro(consulta);
		}catch(SQLException e) {
			log.error("Error al ingresar una categoria");
		}
	}
	
	public  List<Categoria> buscarTodos(){
		String consulta = "SELECT id_categoria, descripcion  FROM categoria";
		List<Categoria> listaCategoria=null;
		DataBaseHelper<Categoria> dataHelper= new DataBaseHelper<Categoria>();
		listaCategoria = dataHelper.
				seleccionarRegistros(consulta, Categoria.class);
		return listaCategoria;
	}
	
	public void borrar(Categoria categoria) {
		String consulta = "DELETE FROM categoria where id_categoria="
				+ categoria.getId();
		DataBaseHelper dataHelper= new DataBaseHelper();
		
		try {
			dataHelper.modificarRegistro(consulta);
			}catch(SQLException e) {
				log.error("Error al ingresar una categoria");
			}
		
	}
}
