package com.arquitecturajava.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.arquitectura.Categoria;
import com.arquitectura.Libro;
import com.arquitecturajava.dao.LibroDAO;

public class LibroDAOHibernateImpl implements LibroDAO{

	public  void insertar(Libro libro) {
		SessionFactory sesionFactoria = HibernateHelper.getSessionFactory();
		Session sesion = sesionFactoria.openSession();
		sesion.beginTransaction();
		sesion.save(libro);
		sesion.getTransaction().commit();
	}
	public  void borrar(Libro libro) {
		SessionFactory factoriaSesion = HibernateHelper.getSessionFactory();
		Session sesion = factoriaSesion.openSession();
		sesion.beginTransaction();
		sesion.delete(libro);
		sesion.getTransaction().commit();
	}
	public  List<Libro> buscarPorCategoria(Categoria categoria){
		List<Libro> libros = null;
		String cosulta = "FROM Libro libros WHERE libros.categoria.id=:categoria";
		SessionFactory factorySesion = HibernateHelper.getSessionFactory();
		Session sesion = factorySesion.openSession();
		Query query = sesion.createQuery(cosulta);
		query.setInteger("categoria", categoria.getId());
		libros = query.list();
		sesion.close();
		return libros;
	}
	public  Libro buscarPorClave(String isbn) {
		Libro libro= null;
		SessionFactory factoriaSesion = HibernateHelper.getSessionFactory();
		Session sesion = factoriaSesion.openSession();
		libro = (Libro) sesion.get(Libro.class,isbn);
		return libro;
	}
	
	public  void salvar(Libro libro) {
		SessionFactory sesionFactoria = HibernateHelper.getSessionFactory();
		Session sesion = sesionFactoria.openSession();
		sesion.beginTransaction();
		sesion.saveOrUpdate(libro);
		sesion.getTransaction().commit();
		
	}
	public   List<Libro> buscarTodos(){
		List<Libro> libros = null;
		String consulta = "FROM Libro libros right join fetch libros.categoria";
		SessionFactory sesionFactoria = HibernateHelper.getSessionFactory();
		Session sesion = sesionFactoria.openSession();
		libros = sesion.createQuery(consulta).list();
		return libros;
		}
	 
	}
