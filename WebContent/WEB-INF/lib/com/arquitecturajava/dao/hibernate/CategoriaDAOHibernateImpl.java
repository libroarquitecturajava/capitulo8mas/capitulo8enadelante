package com.arquitecturajava.dao.hibernate;

import java.util.List;

import com.arquitectura.Categoria;
import com.arquitectura.Libro;
import com.arquitecturajava.dao.CategoriaDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;


public class CategoriaDAOHibernateImpl implements CategoriaDAO{
	
	
	public Categoria buscarPorClave(Integer id) {
		Categoria categoria = null;
		SessionFactory secFatory= HibernateHelper.getSessionFactory();
		Session sesion = secFatory.openSession();
		categoria = (Categoria)sesion.get(Categoria.class, id);
	///	categoria = sesion.get(Categoria.class,id);
		sesion.close();
		return categoria;
	}
	
	public   void insertar(Categoria categoria) {
		SessionFactory sesionFactoria =  HibernateHelper.getSessionFactory();
		Session sesion = sesionFactoria.openSession();
		sesion.getTransaction();
		sesion.save(categoria);
		sesion.getTransaction().commit();
	}
	
	public  List<Categoria> buscarTodos(){
		List<Categoria> lista=null;
		SessionFactory factoria = HibernateHelper.getSessionFactory();
		Session sesion = factoria.openSession();
		lista = sesion.createQuery("FROM Categoria categorias").list();
		sesion.close();
		return lista;
	}
	
	public  void borrar(Categoria categoria) {
		SessionFactory factoriaSesion = HibernateHelper.getSessionFactory();
		Session sesion = factoriaSesion.openSession();
		sesion.beginTransaction();
		sesion.delete(categoria);
		sesion.getTransaction().commit();
	}
}
