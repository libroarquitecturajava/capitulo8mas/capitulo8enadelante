package com.arquitecturajava.dao.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class HibernateHelper {

	private static final SessionFactory sesionFactoria = buildSessionFactory();
	
	
	private static SessionFactory buildSessionFactory() {
		SessionFactory sf= null;
		try {
		sf = new Configuration().configure().buildSessionFactory();
		
		}catch(Throwable ex) {
			System.err.println("Error al crear la factoria de session. "+ ex);
			throw new ExceptionInInitializerError(ex);
		}
		return sf;
	}
	
	public static SessionFactory getSessionFactory() {
		return sesionFactoria;
	}
}
