package com.arquitecturajava.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.arquitecturajava.dao.JDBC.LibroDAOJDBCImpl;
import com.arquitecturajava.dao.hibernate.LibroDAOHibernateImpl;
import com.arquitecturajava.dao.jpa.LibroDAOJPAImpJPAGeneric;


/* Esta clase permite elegir que tipo de familia de peristencia 
 * ya sea JPA o Hibernate*/
public abstract class DAOAbstractFactory {

	private static String  tipoPersistencia = obtenerTipoPersistencia(); 
	
	private static final Logger log = Logger.
			getLogger(DAOAbstractFactory.class.getPackage().getName());
	
	private static  String obtenerTipoPersistencia() {
		String persistencia=null;
		try {
			Properties p = new Properties();
			System.out.println(new File ("./").getAbsolutePath());
			p.load(
					DAOAbstractFactory.class.getResourceAsStream(
					"confPersistencia.properties")
					);
			persistencia = p.getProperty("TecnologiaPeristencia");
		}catch(FileNotFoundException e) {
			log.error("No se encontro el archivo de configuracion");	
		}catch(IOException e) {
			log.error("No se encontro el archivo de configuracion");
		}
		return persistencia;
	}
	
	public static DAOFactory getInstance() {
		DAOFactory fabrica = null;
		
		if(tipoPersistencia.contains("JPA")) {
			fabrica = new DAOJPAFactory();
		}else if(tipoPersistencia.contains("Hibernate")) {
			fabrica = new DAOHibernateFactory();
		}
		return fabrica;
	}
}
