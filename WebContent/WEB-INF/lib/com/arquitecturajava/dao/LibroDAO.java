package com.arquitecturajava.dao;

import java.util.List;

import com.arquitectura.Categoria;
import com.arquitectura.Libro;

public interface LibroDAO {

	public abstract void insertar(Libro libro);
	public abstract void borrar(Libro libro);
	public abstract List<Libro> buscarPorCategoria(Categoria categoria);
	public abstract Libro buscarPorClave(String isbn); 
	public abstract void salvar(Libro libro);
	public abstract  List<Libro> buscarTodos(); 
}
