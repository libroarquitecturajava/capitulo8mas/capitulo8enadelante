package com.arquitecturajava.servicios;

import java.util.List;
import com.arquitectura.Categoria;
import com.arquitectura.Libro;
import com.arquitecturajava.dao.CategoriaDAO;
import com.arquitecturajava.dao.LibroDAO;
public interface ServiciosLibros {
	 public void salvarLibros(Libro libro);
	 public void borrarLibro(Libro libro);
	 public List<Libro> buscarTodosLosLibros();
	 public List<Categoria> buscarCategoriasLibros();
	 public Libro buscarLibroPorClave(String isbn);
	 public Categoria buscarCategoriaPorClave(int id);
	 public List<Libro> buscarLibrosPorCategoria(int categoria);
	 
	 public void setLibroDAO(LibroDAO libroDAO);
	 public LibroDAO getLibroDAO();
	 public void setCategoriaDAO(CategoriaDAO categoriaDAO);
	 public CategoriaDAO getCategoriaDAO();
}
