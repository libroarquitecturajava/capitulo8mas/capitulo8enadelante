package com.arquitecturajava.servicios.impl;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import com.arquitectura.Categoria;
import com.arquitectura.Libro;
import com.arquitecturajava.dao.CategoriaDAO;
import com.arquitecturajava.dao.DAOAbstractFactory;
import com.arquitecturajava.dao.DAOFactory;
import com.arquitecturajava.dao.LibroDAO;
import com.arquitecturajava.servicios.ServiciosLibros;

public class ServicioLibrosImpl implements ServiciosLibros {

	private LibroDAO libroDAO = null;
	private CategoriaDAO categoriaDAO = null;
	
	public ServicioLibrosImpl() {
		super();
//		DAOFactory factoria = DAOAbstractFactory.getInstance();
//		libroDAO = factoria.getLibroDAO();
//		categoriaDAO = factoria.getCategoriaDAO();
	}
	
	@Transactional
	public void salvarLibros(Libro libro) {
		libroDAO.salvar(libro);
	}
	
	@Transactional
	public void borrarLibro(Libro libro) {
		libroDAO.borrar(libro);
	}
	
	@Transactional
	public List<Libro> buscarTodosLosLibros(){
		return libroDAO.buscarTodos();
				
	}
	
	@Transactional
	public List<Categoria> buscarCategoriasLibros(){
		return categoriaDAO.buscarTodos();
	}
	
	@Transactional
	public  Libro buscarLibroPorClave(String isbn) {
		return libroDAO.buscarPorClave(isbn);
	}
	
	@Transactional
	public Categoria buscarCategoriaPorClave(int id) {
		return categoriaDAO.buscarPorClave(new Integer(id));
	}
	
	@Transactional
	public List<Libro> buscarLibrosPorCategoria(int id){
			Categoria categoria = categoriaDAO.buscarPorClave(new Integer(id));
	return libroDAO.buscarPorCategoria(categoria);
	
	}

	public LibroDAO getLibroDAO() {
		return libroDAO;
	}

	public void setLibroDAO(LibroDAO libroDAO) {
		this.libroDAO = libroDAO;
	}

	public CategoriaDAO getCategoriaDAO() {
		return categoriaDAO;
	}

	public void setCategoriaDAO(CategoriaDAO categoriaDAO) {
		this.categoriaDAO = categoriaDAO;
	}
	
	

}
