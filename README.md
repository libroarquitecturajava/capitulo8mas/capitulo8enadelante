# capitulo8enadelante

Capitulo 17

En este capitulo se agrega a la factoria de Spring la  instancia de la clase de servicio, 
de tal manera que Spring instancie las dependecias del proyecto Web.

Al realizar la modificación nos percatamos de que el servicio necesita de las instancias
 de los DAO en este caso son (LibroDAO y CategoriaDAO) por lo que se requiere que se inyecten
 esas dependencias mediante spring. Para poder realizar esta tarea es necesario que se agregen
 los metodos get y set a la implementacion e interface, y posteriormente inyectarlos mediante 
 la etiqueta property, cabe mencionar que el id de la propiedad debe coincidir con el nombre de la 
 variable de los DAOs y de igual manera el get y set. 
 
 Por otra parte en el proyecto se llaman a la clase ClassPathXmlApplicationContext la cual se utilizaba
 para utilizar el archivo de configuracion de spring, probocando un error ya que la configuracion de spring
 se instanciaba en cada ejecucion del metodo getBean de la clase Accion, es por ello que se cambia por la clase
 WEbApplicationContext que permite utilizar la intanciia de la configuracion una vez en todo el proyecto.
 
 Se agrego la dependencia de hibernate-entitymanager version 4.3.11.Final ya que esta tenia la clase 
 org/hibernate/ejb/HibernatePersistence que utiliza para inyectar la cofiguracion de hibernate para 
 la conexion de la base e datos  mediante hibernate y JPA org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
 
 Ya que estas dependenciasse inyectaron en las clases que utilizaban las implementaciones de los DAOJPAImplGeneric, en estas 
 clases se agregaron los gets y sets correspondientes de tal manera que permitiera la inyeccion, otra de las dependencias 
 que no se encuentran en el libro y fueron necesarias son las siguientes:
 
 <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-jdbc</artifactId>
        <version>3.0.5.RELEASE</version>
    </dependency>
    
    <dependency>
   <groupId>org.springframework</groupId>
   <artifactId>spring-orm</artifactId>
   <version>3.0.5.RELEASE</version>
	</dependency>
	y se cambio la version
	  <dependency>
	<groupId>org.hibernate</groupId>
	<artifactId>hibernate-entitymanager</artifactId>
	<!--<version>5.5.4.Final</version>-->
	<version>4.3.11.Final</version>
</dependency>

Concluyendo que las dependencias que se inyectaron mediante Spring fue las dependencias de la base de datos llamada "fuenteDeDatos"
La cual se inyecta en la propiedad EntityManagerFactory  (Es la clase que utiliza JPA para crear la factoria de la conexion), por otro
lado se inyecta la configuracion de Persistencia de JPA creada en el archivo "peristence.xml" llamada persistenceUnitName, por ultimo se inyecta la clase
org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter que recibe la configuraciones de hibernate para el dialecto.

La propiedad de EntityManagerFactory se inyecta a los DAOs, y etos a su vez se inyectan en el servicio de tal manera que es posible 
inyectar las dependencias de las clase de se utilizan en el proyecto.

************************************************************************************************************************************
Capitulo 18 El principio DRY y Spring Templates

	En este capitulo se intenta aplicar el principio DRY en los metodos CRUD del DAO generico, mediante el princpio Template
	que consiste en crear una clase Abstracta que contenga los metodos repetitivos o el mismo comportamiento en las clases en
	este caso se predenta en los DAO,, la clase Abstracta se utiliza como una pllantilla y posteriormente se hereda para que 
	las clases hijas utilicen el mismo comportamiento y solo se agreguela diferencia entre ellas. 
	
	En este caso lo que se desea quitar es el proceso de transacciones que  utiliza JPA ya que repite un comportamiento al re-
	alizar cada transaccion, como se muestra a contiuacion:
	
	..Obtener EntityManagerFactory
	.. Abrir una nueva transaccion
	.. Ejecutar/Abortar Transaccion
	.. Cerrar la coneccion
	
	Una entendido el proceso se utiliza una clase de Spring que internamente ya tiene la secuencia del proceso excepto la operacion
	del CRUD, esta clase se llama JPATemplate, esta se inyecta en el DAO Generico que se llama "GenericDAOJPAImpl" y por medio del
	archivo de configuracion de spring se inyecta el objecto EntityManagerFactory a JpaTemplate y despues se inyecta JPATemplate a
	el DAO.
	
	Existe otra forma mas simplificada, utilizando la clase JDADAOSupport que contiene la clase JPATemplate internamente, por lo que
	solo es necesario heredar la clase JPADAOSupport a la clase "GenericDAOJPAImpl" y por ultimo se modifica los metodos del CRUD y el
	archivo de configuracion al tener la herencia el objecto EntityManagerFactory que utiliza JPA solo se inyecta como estaba anteriormente
	. Cabe mencionar que la aplicacion ya no reaiza las operaciones transaccionales las cuales son guardar y borrar en el siguiente 
	capitulo, esto se debe a que as tracsacciones de la session aunn no se cierran y lo se realiza ninguna operación CRUD, en el
	siguiente capitulo se explicará como solcionarlo.
 
 ************************************************************************************************************************************
 Cápitulo 19 Programación Orientada a Aspecto (AOP)
 
 En este cápitulo se utiliza la progrmación orientada a Aspectos la cual nos permite agregar funcionalidad en otra clase separando
  las tareas que son dependientes de ciertas clases, en nuestro caso el proyecto presenta una dependencia en las transacciones, entre 
  cada operación CRUD, de tal manera que para poder realizar ambas es necesario que la transaccionalidad se haga de manera atómica 
  (Centralizar la gestión de transacciones ) es decir  o se realizan ambas operaciones o ninguna. Y esté  es el motivo del problema, 
  es necesario definir una transacción que agrupe operaciones de varios métodos  de forma conjunta. 
  
  Uno de los patrones de diseño de la programación orientada a Aspectos es el patrón Proxy. Esté establece que las tareas de intermediario
  entre un objeto y su cliente permitiendo controlar el acceso a él añadiendo o modificando la  funcionalidad de forma transparente.
  Este paatrón va de la mano con el uso de interfaces de tal manera si se desea modificar o agregar un nuevo comportamiento sea
  una implementación de las clase la que agregue esta funcionalidad, llegando a ser tranpatente en el caso que se utilice una 
  factoria.
  
  En nuestro proyecto spring nos funciona como una factoria,   nos permite utilizar proxys de Transaccion, Cache, seguridad y otros. 
  En este capitul lo utilizaremos para la trnsaccionalidad, en nuestra aplicación las transacciones son necesarias en la clase de
  servicios, DAOS. Para generar los proxies spring utiliza un tecnica llamada proxies dinámicos, estos   son creados en tiempo
  de ejecución, estos e relacionan con los objetos. 
  
  Su funcionamiento es el siguiente:
  1.- Spring lee los beans de las configuraciones del archivo de configuración del contextoAplicacion.xml
  2.- Crea (instancia) los objetos que necesita la factoria spring.
  3.- Crea los proxies necesarios.
  4.- Asocia los proxies al objeto real en memoría.
  
  La fucionalidad que se agrega a un proxy se le conoce como aspecto. A continuación se muestra un esquema que explica la operación
  del proxie de transacionalidad.
  
  Cliente -> invoca a Proxy -> delega  Objeto
						|  ejecuta
						V
						Gestor T  Gestor Transaccional
						
  Para poder utilizar los proxies es necesario modificar el archivo de configuración contextoAplicacion.xml y las clases de servicio
  y DAOS. El archivo de configuración se modifica para agregar el driver que utiliza spring para la transaccionalidad,la clase encargada 
  es JpaTransactionManager, se agrega la linea <tx:anotation-driven> y su name space para que pueda utilizar el prefijo y spring entienda
  la etiqueta,cabe mencionar que la version del dialecto se tuv que cambiar ya que las versiones de spring que se utilizaron no reconocian
  el dilecto Mysql8Dialect y se cambio al Mysql5Dialecto.
  La clase de la EntityManagerFactory utilizada por JPA y se debe de inyectar en la clase JpaTransactionManager.
  
  Las anotaciones de transaccionalidad son aportaas por el framework Spring y se encargn de definir la transaccionalidad para cada uno
  de los métodos de nuestras clases de persistencia. A continuación se muestra una lista con los distintos tipos de transacciones
  soportadas por sprig:
  
  1.- Required: Se requiere de una transacción. si existe una transacción en curso, el método se ejecutará dentro de ella si no,
				el método se encargará de iniciar un nueva.
			
  2.- Requird_New: Requiere ejecutarse de forma transaccional, si existe unaa transaccion creada, suspenderá la que esta en curso.
				   En el caso de no existir unatransacción, creará una.
				   
  3.- Supports: Si existe una transacción,se ejecutará dentro de ella. Si no exise, no iniciará ninguna.
	
  4.- Not_Supported: Se ejecutará fuera de una trasacción. Si esta transacción existe, será suspendida.
  
  5.- Never: Debe ejecutarse siemre de forma no transactional. Si existe una transacción, lanzará una exception.
  
  6.- Mandatory: Debe ejecutare obligatoriamente  de forma transaccional. Si no lo hace, lanzará una excepción.
  
  7.- Nested: Creará una nueva transacción dentro de la transaccion acutual.
  
  En el capitulo se realizan dos formas para utilizar la transaccionalidad con spring una es mediante la clase JpaTemplate como una
  clase que se encuentra en nuestro DAO generico y porterior mente utilizarlo para llamar a los métodos que nos  permiten hacer la 
  persistencia (persist(objeto), merge(objeto), remove(merge(objeto))).
  
  La otra forma es utilizando herencia, extendiendo la clase JpaDaoSupport, la cual contiene el objeto JpaTemplete. La siguente
  modificación consite en agregar la annotación ya sea @Transactional o @Transactional(readOnly=true, para poder utilzar estas
  anotaciones es necesario implementar la clase de spring org.spring.transaction.annotation.Transactional. esta permite indicar
  al proxy cuales objetos es necesario utilizar la transaccionalidad. Las anotaciones se agregan en la clase de servicio y 
  la clases de GenericDAOJPAImpl.
  
  
 Modificaciones fuera del capitulo, Las modificaciones que se realizaron para que funcionará los cambios fuerón en el cambio de 
 las versiones de la Api de hibernate-core la version 5.1.0.Final truena conla de spring 3.05.RELASE, con el siguiente error.
  error :
  nested exception is java.lang.NoClassDefFoundError: org/hibernate/boot/registry/classloading/internal/ClassLoaderServiceImpl$Work
  Solución:
  https://stackoverflow.com/questions/44438228/nested-exception-is-java-lang-noclassdeffounderror-org-hibernate-boot-registry
  
  Una vez que se cambio el Api de hibernate-core a la version 5.2.12.Final se obtuvo el siguiente error:
  
  error:
  nested exception is java.lang.NoClassDefFoundError: org/hibernate/ejb/HibernatePersistence
  Solución:
  https://stackoverflow.com/questions/17053668/nested-exception-is-java-lang-noclassdeffounderror-org-hibernate-ejb-hibernatep
  
  Para poder darle soucion se concluyo con la version 4.3.8.Final, con esta solo se presento el problema del dialecto de hibernate 
  Mysql8Dialect que se cambio en el archivo de hibernate persistence.xml y en el archivo de configuración de spring contextoAplicacion.xml.
  
  Una vez realizado estos cambios la aplicacion web ya funciono de manera funcional, utilizando el patrón proxy para la transaccionalidad. 
  
  
  
  
 