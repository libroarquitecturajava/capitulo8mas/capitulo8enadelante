<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.arquitectura.Libro"%>
<%@ page import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="${contextPath}/CSS/formato.css"/>
<title>Lista de Libros</title>
</head>
<body>
<form id="formularioFiltros" action="FiltrarLibrosPorCategoria.do">
<fieldset>
<legend>Libros</legend>
<select name="categoria">
<option value="seleccionar"> seleccionar</option>

				
	<c:forEach var="categoria" items="${listaDeCategorias}">
		<option value="${categoria.id}">${categoria.descripcion}</option>
	</c:forEach>
			</select>
			<input type="submit" value="filtrar"/> </fieldset></form>
<br/>		
<fieldset>
	<c:forEach var="book" items="${listaDeLibros}">	
			${book.isbn}
			${book.titulo}
			${book.categoria.descripcion}
			<a href="BorrarLibro.do?isbn=${book.isbn}">Borrar</a>
			<a href="FormularioEditarLibro.do?isbn=${book.isbn}">Editar</a>
			<br/>
	</c:forEach>
</fieldset>			

<a href="FormularioInsertarLibro.do">insertar Libro</a> 


</body>
</html>